from flask import Flask



app = Flask(__name__)



users=[
    {
        "id":1,
        "name":"john",
        "age":20
    },
    {
        "id":2,
        "name":"Alice",
        "age":40
    },
    {
        "id":3,
        "name":"Ronny",
        "age":25
    },
    {
        "id":4,
        "name":"Madelaine",
        "age":23
    }
]



@app.route('/health')
def hello_world():
   return "i'm healthy"



@app.route('/user')
def get_users():
   return users



@app.route('/user/<id>')
def get_user(id):
   user=""
   for u in users:
        if u["id"]==int(id):
            user=u
   return user



if __name__ == '__main__':
   app.run(host="0.0.0.0",port=5000)