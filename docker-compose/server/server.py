from flask import Flask
import os
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor

app = Flask(__name__)

trace.set_tracer_provider(
TracerProvider(
        resource=Resource.create({SERVICE_NAME: "server-api"})
    )
)
tracer = trace.get_tracer(__name__)
otlps_exporter = OTLPSpanExporter(
    endpoint=os.getenv("OTLC"), insecure=True
)
span_processor = BatchSpanProcessor(otlps_exporter)
trace.get_tracer_provider().add_span_processor(span_processor)

users=[
    {
        "id":1,
        "name":"john",
        "age":20
    },
    {
        "id":2,
        "name":"Alice",
        "age":40
    },
    {
        "id":3,
        "name":"Ronny",
        "age":25
    },
    {
        "id":4,
        "name":"Madelaine",
        "age":23
    }
]

@app.route("/health")
def check_health():
    with tracer.start_as_current_span('health'):
        return {"message":"ok"}


@app.route("/user")
def get_data():
    with tracer.start_as_current_span('user'):
        return users


if __name__=='__main__':
    app.run(host='0.0.0.0',port=5000)